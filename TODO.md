# Add SPDX license identifier to all files

- clarify license for following contributions
    - Messagebox: Kimmo Lindholm, https://github.com/kimmoli/paint/blob/master/qml/components/Messagebox.qml
    - Filtered ListView: https://github.com/oKcerG/SortFilterProxyModel
    - Sports-Tracker support: niemisenjussi
    - Strava support: piggz

# Release Procedure

- set changelog and release date to harbour-kuri.changes file
- compile with translations and complete translation to german language
- set new version number to yaml file (spec file will be automatically updated)
- clean project and compile on release for arm and 486
    - without a clean build, the displayed version number might not get updated if no C++ source file was touched after setting the version number

# Features

- create gitlab issues or all items in this list
- introduce lap function
- Implement shoe management for running
- show battery state of phone with charging icon on record page
- add max pace to detailspage/trackloader
- look into old map tiles folder and delete files there. Ask user!
- map on record page: zoom into map automatically. Save and restore last zoom setting.
- add new feature: autopause if speed goes below certain threshold
- add icons for indicating over/below average speed, average pace and average heart rate to record page
- add min/max elevation to detailspage/trackloader
- fix elevation up/down bad values. This is a big thing. Elevation data needs to be corrected with appropriate algorithms.
- check if speedoverground in QGeoPositionInfo is eventually better then own speed calculation -> seems to contain no information
- add ghost target to track progress
- implement workout comparison. Compare current workout with virtual workout in the past
- support all workout types from meerun (is not possible because export function in meerun is broken for many workout types, even on android)
- save pause time to gpx file. Look in Meerun gpx file where to save pause data.
- Style first page list view like in meerun (upper part of page should not scroll)
- introduce reward system:
    - fastest run 5/10/20/40 km
    - longest run (distance/time)
    - most workouts in a week/month
    - most distance in a week/month
    - most time in a week/month
    -  5km in under 30/25/20/15 mins
    - 10km in under 1h/55/50/48/45/40/35/30 mins
- view burned calories on mainpage and recordpage and detailspage
- calculate burned calories -> https://www.nutristrategy.com/caloriesburned.htm
- introduce female voice
- alert system:
    - vibration alert / sound alert
    - send vibration also to pebble via dbus call provided by rockpool (does not exist. Have to do this myself?)
    - make sound file configurable or changeable
- view diagrams on the statistics page. See app "Rush Hour" and "SystemDataScope" which are on openrepos.
- make volume of audio messages adjustable
- check if bluetooth is active. If not, switch it on. (see file ActionDisableBluetooth.qml in app slumber-master, or rockpool.qml)
- support workouts without gps e.g. indoor activities
- BLE heart rate devices support (currently waiting for kimmoli to bring this to OPX)

# Bugs

- create gitlab issues or all items in this list
- [check, high]when loading autosave and while not resuming workout in recordpage, the duration gets smaller
- sometimes the mediaplayer would not set playing to false. Need a watchdog for that.
- meerun imports have no max. speed
- bluetooth data logging function (writing to files does not work???)
- on the map in recordpage there are stragne horizontal lines coming from tracklines
- there are strange files in the ~/Laufhelden folder, e.g. "running-Di. Nov. 7 16:00:36 2017-7.5km.gpx.wZ8116" or "running-Mi. Nov. 8 15:46:48 2017-0.5km.gpx.M27425"
- sometimes there is a view problem on mainpage after a completed workout.
- if HRM is active and record page is left and then opened again, we get a BT error.
- after bt scan and finding new HRM device, not able to connect. First need to restart app.
- compiler issue, comparison between signed and unsigned
- write to log file is not working, strange no error or something...
- sometimes, maybe if app is left on cover view or even in background, screen on is not longer working
