/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../fields/FieldLayout.js" as FieldLayout

Dialog
{
    id: root

    property QtObject field

    onAccepted: {
        var index = fieldSelection.currentIndex
        if(index >= 0) {
            FieldLayout.layout[field.row][field.column].source = fieldTypes.get(index).source
            field.update()
        }
    }


    ListModel {
        id: fieldTypes
        ListElement {
            text: qsTr("Duration")
            source: "RecordPageField_Duration.qml"
        }
        ListElement {
            text: qsTr("Stage Time")
            source: "RecordPageField_Duration_Lap.qml"
        }
        ListElement {
            text: qsTr("Distance")
            source: "RecordPageField_Distance.qml"
        }
        ListElement {
            text: qsTr("Pace")
            source: "RecordPageField_Pace.qml"
        }
        ListElement {
            text: qsTr("Heart Rate")
            source: "RecordPageField_HeartRate.qml"
        }
        ListElement {
            text: qsTr("Calories")
            source: "RecordPageField_Calories.qml"
        }
        ListElement {
            text: qsTr("Elevation (up)")
            source: "RecordPageField_Elevation_Up.qml"
        }
        ListElement {
            text: qsTr("Elevation (down)")
            source: "RecordPageField_Elevation_Down.qml"
        }
    }

    Component {
        id: highlightItem
        Rectangle {
            width: root.width
            height: Theme.itemSizeSmall
            color: Theme.highlightBackgroundColor
            opacity: Theme.highlightBackgroundOpacity

            y: fieldSelection.currentItem.y
            Behavior on y {
               SpringAnimation {
                   spring: 0
                   damping: 0
                   duration: 50
               }
           }
        }
    }


    DialogHeader {
        id: dialogHeader
    }

    SilicaListView {
        id: fieldSelection
        anchors.top: dialogHeader.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        model: fieldTypes
        header: SectionHeader { text: qsTr("Field Types") }

        highlight: highlightItem
        currentIndex: -1

        delegate: MouseArea {
            width: ListView.view.width
            height: Theme.itemSizeSmall

            Label {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin
                text: model.text
                color: Theme.primaryColor
            }

            onClicked: fieldSelection.currentIndex = index
        }

        VerticalScrollDecorator {}
    }

    Component.onCompleted: {
        for(var i = 0; i < fieldTypes.count; i++) {
            if(fieldTypes.get(i).source === FieldLayout.layout[field.row][field.column].source) {
                fieldSelection.currentIndex = i;
            }
        }
    }
}
