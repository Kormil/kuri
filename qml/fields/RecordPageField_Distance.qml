/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import QtQml 2.2

RecordPageField {
    id: root

    function update() {
        value = (recorder.distance / 1000.).toFixed(2);
    }

    label: qsTr("Distance")
    value: "0.00"
    unit: qsTr("km")

    Component.onCompleted: {
        recorder.distanceChanged.connect(root.update);
    }

    Component.onDestruction: {
        recorder.distanceChanged.disconnect(root.update);
    }
}
