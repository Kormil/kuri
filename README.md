# Kuri

Kuri is a sport tracking application for Sailfish OS. Kuri means "run" in Esperanto. Funnily it also means "discipline" in Finnish.
Features are:
- recording tracks
- voice coach
- view recorded track on a map and show statistics
- save track as GPX file
- connecting to bluetooth heart rate device
- uploading to Strava and Sport-Tracker.com
- Pebble support (needs Rockpool)
- and more...

This application originates from a fork of Laufhelden by Jens Drescher: https://github.com/jdrescher2006/Laufhelden

# Credits

- Jens Dresher and all the contributors of Laufhelden

Workout icons are from here: https://de.icons8.com/ They are under this license: https://creativecommons.org/licenses/by-nd/3.0/
Filtered ListView is named "SortFilterProxyModel" and is under MIT license: https://github.com/oKcerG/SortFilterProxyModel/blob/master/LICENSE

# License

License: GNU General Public License (GNU GPLv3)

# Copyright

Copyright 2020 Mathias Kraus

# Development
- mapbox-gl
    - download from https://openrepos.net/content/rinigus/mapbox-gl-native-bindings-qt-qml
    - copy to emulator: scp -P 2223 -i ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-latest/root mapbox-gl.rmp root@localhost:
    - log in to emulator: ssh -p 2223 -i ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-latest/root root@localhost
    - install in emulator: rpm -i mapbox-gl.rpm
