# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-kuri

include (o2/src/src.pri)

# Define the preprocessor macro to get the application version in our application.
DEFINES += APP_VERSION=\"\\\"$${VERSION}\\\"\"

CONFIG += c++11
CONFIG += sailfishapp
QT += positioning location concurrent
QT += bluetooth sensors
QT += dbus

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

SOURCES += \
    src/device.cpp \
    src/deviceinfo.cpp \
    src/harbour-kuri.cpp \
    src/historymodel.cpp \
    src/light.cpp \
    src/logwriter.cpp \
    src/pebblemanagercomm.cpp \
    src/pebblewatchcomm.cpp \
    src/plotwidget.cpp \
    src/serviceinfo.cpp \
    src/settings.cpp \
    src/timeformatter.cpp \
    src/trackloader.cpp \
    src/trackrecorder.cpp

HEADERS += \
    src/device.h \
    src/deviceinfo.h \
    src/historymodel.h \
    src/light.h \
    src/logwriter.h \
    src/pebblemanagercomm.h \
    src/pebblewatchcomm.h \
    src/plotwidget.h \
    src/serviceinfo.h \
    src/settings.h \
    src/timeformatter.h \
    src/trackloader.h \
    src/trackrecorder.h

OTHER_FILES += \
    rpm/harbour-kuri.changes \
    rpm/harbour-kuri.spec \
    rpm/harbour-kuri.yaml

DISTFILES += \
    harbour-kuri.desktop \
    qml/harbour-kuri.qml \
    qml/kuri.png \
    qml/components/GPSIndicator.qml \
    qml/components/InfoItem.qml \
    qml/components/RecordPageColorTheme.qml \
    qml/components/RecordPageLockScreen.qml \
    qml/components/Map.qml \
    qml/cover/CoverPage.qml \
    \ ## fields ##
    qml/fields/FieldLayout.js \
    qml/fields/FieldUtils.js \
    qml/fields/RecordPageField.qml \
    qml/fields/RecordPageFieldLoader.qml \
    qml/fields/RecordPageField_Calories.qml \
    qml/fields/RecordPageField_Distance.qml \
    qml/fields/RecordPageField_Duration.qml \
    qml/fields/RecordPageField_Duration_Lap.qml \
    qml/fields/RecordPageField_Elevation_Down.qml \
    qml/fields/RecordPageField_Elevation_Up.qml \
    qml/fields/RecordPageField_HeartRate.qml \
    qml/fields/RecordPageField_Pace.qml \
    \ ## graph ##
    qml/graph/Axis.qml \
    qml/graph/GraphData.qml \
    \ ## pages ##
    qml/pages/AboutPage.qml \
    qml/pages/BrowserPage.qml \
    qml/pages/BTConnectPage.qml \
    qml/pages/CoverSettingsPage.qml \
    qml/pages/DetailedViewPage.qml \
    qml/pages/DiagramViewPage.qml \
    qml/pages/MainPage.qml \
    qml/pages/MapSettingsPage.qml \
    qml/pages/MapViewPage.qml \
    qml/pages/MyStravaActivities.qml \
    qml/pages/PebbleSettingsPage.qml \
    qml/pages/PreRecordPage.qml \
    qml/pages/RecordPage.qml \
    qml/pages/RecordPageFieldDialog.qml \
    qml/pages/SaveDialog.qml \
    qml/pages/SettingsMenu.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/SocialMediaMenu.qml \
    qml/pages/SportsTrackerSettingsPage.qml \
    qml/pages/SportsTrackerUploadPage.qml \
    qml/pages/StravaActivityPage.qml \
    qml/pages/StravaComments.qml \
    qml/pages/StravaKudos.qml \
    qml/pages/StravaSegment.qml \
    qml/pages/StravaSegments.qml \
    qml/pages/StravaSettingsPage.qml \
    qml/pages/StravaUploadPage.qml \
    qml/pages/ThresholdSettingsPage.qml \
    qml/pages/VoiceCycleDistanceSettingsPage.qml \
    qml/pages/VoiceCycleDurationSettingsPage.qml \
    qml/pages/VoiceEventsSettingsPage.qml \
    qml/pages/VoiceGeneralSettingsPage.qml \
    qml/pages/VoiceSettingsPage.qml \
    \ ## tools QML ##
    qml/tools/CustomButton.qml \
    qml/tools/DelayedTrigger.qml \
    qml/tools/MediaPlayerControl.qml \
    qml/tools/Messagebox.qml \
    qml/tools/PebbleComm.qml \
    qml/tools/ScreenBlank.qml \
    \ ## tools javascript ##
    qml/tools/JSTools.js \
    qml/tools/RecordPageDisplay.js \
    qml/tools/SharedResources.js \
    qml/tools/SportsTracker.js \
    qml/tools/Thresholds.js \
    \ ## images ##
    qml/img/calendar.png \
    qml/img/circle.png \
    qml/img/cover.png \
    qml/img/elevation.png \
    qml/img/flame.png \
    qml/img/general.png \
    qml/img/heart.png \
    qml/img/icon-lock-error.png \
    qml/img/icon-lock-info.png \
    qml/img/icon-lock-ok.png \
    qml/img/icon-lock-warning.png \
    qml/img/length.png \
    qml/img/map.png \
    qml/img/map_btn_center.png \
    qml/img/map_btn_max.png \
    qml/img/map_btn_min.png \
    qml/img/map_btn_settings.png \
    qml/img/map_pause.png \
    qml/img/map_play.png \
    qml/img/map_resume.png \
    qml/img/map_stop.png \
    qml/img/mountains.png \
    qml/img/pagelocator_1_3.png \
    qml/img/pagelocator_2_3.png \
    qml/img/pagelocator_3_3.png \
    qml/img/pebble.jpg \
    qml/img/pebble.png \
    qml/img/pin.png \
    qml/img/position-circle-blue.png \
    qml/img/socialmedia.png \
    qml/img/speed.png \
    qml/img/speedavg.png \
    qml/img/sportstracker.png \
    qml/img/strava.png \
    qml/img/strava_pr_1.png \
    qml/img/strava_pr_2.png \
    qml/img/strava_pr_3.png \
    qml/img/time.png \
    qml/img/trophy.png \
    qml/img/voicecoach.png \
    \ ## workout icons ##
    qml/workouticons/biking.png \
    qml/workouticons/hiking.png \
    qml/workouticons/mountainBiking.png \
    qml/workouticons/rollerSkating.png \
    qml/workouticons/running.png \
    qml/workouticons/skiing.png \
    qml/workouticons/walking.png \
    \ ## translations ##
    translations/*.ts

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

TRANSLATIONS += \
    translations/harbour-kuri-de.ts \
    translations/harbour-kuri-es.ts \
    translations/harbour-kuri-fi_FI.ts \
    translations/harbour-kuri-fr.ts \
    translations/harbour-kuri-hu.ts \
    translations/harbour-kuri-nl.ts \
    translations/harbour-kuri-nl_BE.ts \
    translations/harbour-kuri-pl.ts \
    translations/harbour-kuri-ru.ts \
    translations/harbour-kuri-sv.ts \
    translations/harbour-kuri-zh_CN.ts


include(libs/qmlsortfilterproxymodel/SortFilterProxyModel.pri)
